# Generated by Django 3.1.4 on 2021-01-26 04:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geriatrics', '0012_auto_20210126_0359'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resident',
            name='diet',
            field=models.CharField(blank=True, max_length=180),
        ),
    ]
