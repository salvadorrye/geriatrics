from django.apps import AppConfig


class GeriatricsConfig(AppConfig):
    name = 'geriatrics'

    def ready(self):
        import geriatrics.signals
