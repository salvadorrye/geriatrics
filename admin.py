from django.contrib import admin
from django.contrib.admin import AdminSite
from .models import Residence, Admission, Discharge, Resident
from kardex.models import OrganSystem, Diagnosis, Kardex, Therapy, PrescribedMedication
from plan_of_care.models import Management
from scanned_docs.models import Document, Category

class GeriatricsAdminSite(AdminSite):
    site_header = 'Camillus MedHaven Geriatrics Admin'
    site_title = 'Camillus MedHaven Geriatrics Admin Portal'
    index_title = 'Welcome to Camillus MedHaven Geriatrics Portal'

geriatrics_admin_site = GeriatricsAdminSite(name='geriatrics_admin')

# Register your models here.
# Kardex
@admin.register(OrganSystem)
class OrganSystemAdmin(admin.ModelAdmin):
    list_display = ['name']
    prepopulated_fields = {'slug': ('name',)}

class DiagnosisInline(admin.TabularInline):
    model = Diagnosis
    search_fields = ['diagnosis']

class PrescribedMedicationInline(admin.TabularInline):
	model = PrescribedMedication
	raw_id_fields = ['medication']

class ManagementInline(admin.TabularInline):
    model = Management
    search_fields = ['title', 'content']

@admin.register(Diagnosis)
class DiagnosisAdmin(admin.ModelAdmin):
	list_display = ['kardex', 'date', 'diagnosis']
	inlines = [PrescribedMedicationInline, ManagementInline]

@admin.register(Kardex)
class KardexAdmin(admin.ModelAdmin):
    search_fields = ['resident__last_name', 'resident__given_name']
    list_display = ['resident', 'known_allergies', 'vital_signs_monitoring']
    list_filter = ['known_allergies', 'vital_signs_monitoring', 'fall', 'pressure_ulcer', 'bleeding', 'aspiration',
            'infection', 'wandering', 'others']
    list_editable = ['known_allergies', 'vital_signs_monitoring']
    fieldsets = (
            ('Kardex', {'fields': ('resident', 'known_allergies', 'vital_signs_monitoring',)}),
            ('Precaution', {
                'fields': (
                    'fall',
                    'pressure_ulcer',
                    'bleeding',
                    'aspiration',
                    'infection',
                    'wandering',
                    'others',
                    'specify',
                    )
                }),
            )
    inlines = [DiagnosisInline]

@admin.register(Therapy)
class TherapyAdmin(admin.ModelAdmin):
    search_fields = ['resident__last_name', 'resident__given_name', 'therapy']
    list_display = ['resident', 'therapy', 'schedule']
    list_filter = ['therapy']
    list_editable = ['therapy', 'schedule']

# Geriatrics
@admin.register(Residence)
class ResidenceAdmin(admin.ModelAdmin):
    list_display = ['name', 'address']

@admin.register(Resident)
class ResidentAdmin(admin.ModelAdmin):
    search_fields = ['given_name', 'last_name', 'address']
    list_display = ['id', 'last_name', 'given_name', 'sex', 'osca_id', 'philhealth_number', 'date_of_birth', 'civil_status', 'reminders', 'diet', 'weight_in_kg']
    list_filter = ['sex', 'date_of_birth', 'civil_status']
    list_editable = ['reminders', 'diet', 'weight_in_kg']
    #filter_horizontal = ('physicians',)
    fieldsets = (
            ('Resident Profile', {
                'fields': (
                    'photo',
                    'last_name',
                    'given_name',
                    'middle_name',
                    'sex',
                    'osca_id',
                    'philhealth_number',
                    'address',
                    'date_of_birth',
                    'birth_place',
                    'nationality',
                    'civil_status',
                    'most_recent_occupation',
                    'religion',
                    'reminders',
                    'diet',
                    'weight_in_kg',
                    'height_in_cm',
                    )
                }),
            )

@admin.register(Admission)
class AdmissionAdmin(admin.ModelAdmin):
    search_fields = ['resident__given_name', 'resident__last_name', 'admitting_diagnosis']
    list_display = ['resident', 'admitting_diagnosis', 'date_of_admission', 'discharged', 'admitted_at', 'admitted_by', 'level_of_care']
    list_filter = ['date_of_admission', 'admitted_at', 'level_of_care', 'discharged', 'admitted_by']
    list_editable = ['admitted_at', 'level_of_care']

@admin.register(Discharge)
class DischargeAdmin(admin.ModelAdmin):
    list_display = ['admission', 'date_of_discharge', 'reason', 'deceased', 'cause_of_death', 'date_of_death']
    list_filter = ['date_of_discharge', 'deceased', 'date_of_death']

#Scanned documents
@admin.register(Document)
class DocumentAdmin(admin.ModelAdmin):
    search_fields = ['resident__last_name', 'resident__given_name', 'name']
    list_display = ['name', 'resident', 'date', 'category', 'remarks']
    list_filter = ['date', 'category']

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}

geriatrics_admin_site.register(OrganSystem, admin_class=OrganSystemAdmin)
geriatrics_admin_site.register(Diagnosis, admin_class=DiagnosisAdmin)
geriatrics_admin_site.register(Kardex, admin_class=KardexAdmin)
geriatrics_admin_site.register(Therapy, admin_class=TherapyAdmin)

geriatrics_admin_site.register(Residence, admin_class=ResidenceAdmin)
geriatrics_admin_site.register(Resident, admin_class=ResidentAdmin)
geriatrics_admin_site.register(Admission, admin_class=AdmissionAdmin)
geriatrics_admin_site.register(Discharge, admin_class=DischargeAdmin)

geriatrics_admin_site.register(Document, admin_class=DocumentAdmin)
geriatrics_admin_site.register(Category, admin_class=CategoryAdmin)


