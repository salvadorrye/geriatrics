import django_tables2 as tables
from .models import Admission, Resident
from django.utils.html import format_html

class AdmissionsTable(tables.Table):
    class Meta:
        model = Admission
        template_name = 'core/bootstrap.html'
        sequence = ('resident', 'admitting_diagnosis', 'date_of_admission', 'admitted_at', 'level_of_care',)
        exclude = ('id', 'time_of_admission', 'discharged', 'admitted_by',)

    def render_resident(self, value):
        birthdays_today = Resident.objects.get_birthdays()
        upcoming_birthdays = Resident.objects.get_upcoming_birthdays()
        if value in birthdays_today:
            bday = f'Birthday today!'
        elif value in upcoming_birthdays:
            bday = f'Upcoming birthday - {value.date_of_birth.strftime("%B %d")}'
        else:
            bday = ''
        if bday != '':
            s = f'<i class="fas fa-birthday-cake" title="{bday}"></i> <a href="{value.admission.get_absolute_url()}">{value}</a>'
        else:
            s = f'<a href="{value.admission.get_absolute_url()}">{value}</a>'
        return format_html(s)


