from django.views import generic
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from .models import Residence, Admission
from .tables import AdmissionsTable
from patient_monitoring.tables import VitalSignsTable, FluidIntakeAndOutputTable, TurningScheduleTable
from patient_monitoring.models import VitalSigns, FluidIntakeAndOutput, TurningSchedule
from plan_of_care.tables import PlanOfCareTable
from plan_of_care.models import PlanOfCare
from medication.tables import MedicationTable
from medication.models import Medication
from kardex.models import OrganSystem
from django_tables2 import MultiTableMixin
from django.http import HttpResponseForbidden

# Create your views here.
"""def admissions(request, residence_id=None):
    residence = None
    residences = Residence.objects.all()
    admissions = Admission.objects.filter(discharged=False)
    if residence_id:
        residence = get_object_or_404(Residence, id=residence_id)
        admissions = admissions.filter(residence=residence)
    return render(request, 'geriatrics/admissions/list.html', {'residence': residence, 'residences': residences, 'admissions': admissions})"""

class Admissions(LoginRequiredMixin, generic.ListView):
    model = Admission

    def get_context_data(self, **kwargs):
        context = super(Admissions, self).get_context_data(**kwargs)
        context['title'] = 'Admissions'
        table = AdmissionsTable(Admission.objects.filter(discharged=False))
        table.paginate(page=self.request.GET.get("page", 1), per_page=5)
        context['table'] = table
        return context

class AdmissionDetailView(UserPassesTestMixin, MultiTableMixin, generic.DetailView):
    model = Admission
    tables = []
    table_pagination = {
        "per_page": 10 
    }

    def handle_no_permission(self):
        return HttpResponseForbidden()

    def test_func(self):
        #print(self.request.build_absolute_uri(self.request.get_full_path()))
        obj = super().get_object()
        return self.request.user in obj.relatives.all() or self.request.user.is_staff

    def get_object(self):
        obj = super().get_object()
        resident=obj.id
        vs_table = VitalSignsTable(
                VitalSigns.objects.filter(
                    resident=resident
                    )
                )
        io_table = FluidIntakeAndOutputTable(
                FluidIntakeAndOutput.objects.filter(
                    resident=resident
                    )
                )
        turn_table = TurningScheduleTable(
                TurningSchedule.objects.filter(
                    resident=resident
                    )
                )
        planofcare_table = PlanOfCareTable(
                PlanOfCare.objects.filter(
                    resident=resident
                    )
                )
        medication_table = MedicationTable(
                Medication.objects.filter(
                    resident=resident
                    )
                )
        self.tables.append(vs_table)
        self.tables.append(io_table)
        self.tables.append(turn_table)
        self.tables.append(planofcare_table)
        self.tables.append(medication_table)
        return obj

    def get_context_data(self, **kwargs):
        context = super(AdmissionDetailView, self).get_context_data(**kwargs)
        context['title'] = 'Admission Detail'
        #context['systems'] = OrganSystem.objects.all()
        return context


