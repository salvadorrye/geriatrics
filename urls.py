from django.urls import path
from . import views

app_name = 'geriatrics'

urlpatterns = [
    path('', views.Admissions.as_view(template_name='geriatrics/admissions/admission_list.html'), name='admissions'),
    path('<int:pk>/', views.AdmissionDetailView.as_view(template_name='geriatrics/admissions/admission_detail.html'), name='admission-detail'),
]
