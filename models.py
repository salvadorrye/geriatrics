from django.db import models
from django.urls import reverse
from django.utils.timesince import timesince
from birthday.fields import BirthdayField
from birthday.managers import BirthdayManager
from django.conf import settings

SEX = (
    ('male', 'Male'),
    ('female', 'Female'),
    )

CIVIL_STATUS = (
    ('single', 'Single'),
    ('married', 'Married'),
    ('widowed', 'Widowed'),
    )

LEVEL_OF_CARE = (
    ('high', 'High'),
    ('intermediate', 'Intermediate'),
    ('low', 'Low'),
    )

#Geriatric Models   
class Residence(models.Model):
    name = models.CharField(max_length=70)
    address = models.TextField(max_length=175, blank=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return f'{self.name}'

class Admission(models.Model):
    date_of_admission = models.DateField(auto_now=False, null=False, blank=False)
    time_of_admission = models.TimeField(auto_now=False, null=False, blank=False)
    admitting_diagnosis = models.TextField(blank=True)
    admitted_at = models.ForeignKey('Residence', on_delete=models.PROTECT, null=True)
    resident = models.OneToOneField('Resident', on_delete=models.CASCADE)
    level_of_care = models.CharField(max_length=12, choices=LEVEL_OF_CARE, blank=True)    
    discharged = models.BooleanField(default=False, editable=False)
    admitted_by = models.CharField(max_length=120, blank=True)
    relatives = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True)

    class Meta:
        ordering = ['date_of_admission']

    def __str__(self):
        return f'{self.resident}'

    def get_absolute_url(self):
        return reverse('geriatrics:admission-detail', args=[str(self.id)])

class Discharge(models.Model):
    admission = models.OneToOneField('Admission', on_delete=models.CASCADE)
    date_of_discharge = models.DateField(auto_now=False)
    reason = models.TextField(max_length=175)
    deceased = models.BooleanField(default=False)
    date_of_death = models.DateField(auto_now=False, null=True, blank=True, verbose_name='died_on')
    time_of_death = models.TimeField(auto_now=False, null=True, blank=True)
    cause_of_death = models.TextField(max_length=175, blank=True)

    class Meta:
        ordering = ['date_of_discharge']

    def __str__(self):
        return f'{self.admission.resident} - Discharged: {self.date_of_discharge}'

class Resident(models.Model):
    last_name = models.CharField(max_length=35)
    given_name = models.CharField(max_length=35, verbose_name='name')
    middle_name = models.CharField(max_length=35, blank=True)
    photo = models.ImageField(
            null=True,
            blank=True,
            upload_to="photos/residents/%Y/%m/%D",
            )
    sex = models.CharField(max_length=6, choices=SEX, blank=False)
    osca_id = models.CharField(max_length=24, blank=True)
    philhealth_number = models.CharField(max_length=24, blank=True)
    address = models.TextField(max_length=175)
    #date_of_birth = models.DateField(auto_now=False)
    date_of_birth = BirthdayField()
    objects = BirthdayManager()
    birth_place = models.CharField(max_length=35, blank=True)
    nationality = models.CharField(max_length=70, blank=True)
    civil_status = models.CharField(max_length=7, choices=CIVIL_STATUS, blank=True)
    most_recent_occupation = models.CharField(max_length=70, blank=True)
    religion = models.CharField(max_length=70, blank=True)
    reminders = models.CharField(max_length=180, blank=True)
    diet = models.TextField(blank=True)
    weight_in_kg = models.FloatField(null=True, blank=True)
    height_in_cm = models.FloatField(null=True, blank=True)   

    class Meta:
        ordering = ['last_name', 'given_name']

    def __str__(self):
        return f'{self.last_name}, {self.given_name}' 

