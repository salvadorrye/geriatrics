from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from .models import Admission, Discharge

@receiver(post_save, sender=Discharge)
def discharge_admitted(sender, instance, created, **kwargs):
    if created:
        instance.admission.discharged = True
        instance.admission.save()

@receiver(post_delete, sender=Discharge)
def cancel_discharge_of_admitted(sender, instance, using, **kwargs):
    instance.admission.discharged = False
    instance.admission.save()

